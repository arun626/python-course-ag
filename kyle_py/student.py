# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 14:21:45 2022

@author: kyle
"""
from person import Person
    
class Student(Person):
    def __init__(self, name, last_name, birth_year):
        super().__init__(name, last_name, birth_year)
        self.workshop = None
        
    def enroll(self, workshop):
        self.workshop = workshop
        
    def __str__(self):  #dunder
        return f"student {self.name} {self.last_name}"

if __name__ == '__main__':     
    me = Student('kyle', 'juedes')
    print (me.get_full_name())
    me.enroll('python for the lab')
    print (me.workshop)

