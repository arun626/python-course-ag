# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 13:52:12 2022

@author: kyle
"""

class Person:
    def __init__(self, name = 'clint', last_name = 'eastwood', birth_year = None):
        self.name = name
        self.last_name = last_name
        self.full_name = None
        self.birth_year = birth_year
    
    def get_full_name(self):
        self.full_name = self.name + self.last_name
        return self.full_name 
    
    def calculate_age(self):
        age=2022 - self.birth_year
        return age 


if __name__ == '__main__':
    you = Person('john', 'wayne')
    print (you.name)
    print (you.last_name)
    print (you.full_name)

    his =Person()
    print (his.name)
    print (his.last_name)